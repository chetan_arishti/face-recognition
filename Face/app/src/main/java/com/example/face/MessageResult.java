package com.example.face;

class MessageResult {
    private String result;

    public MessageResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }



}
